# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


在畫面上方有一整列的工作列，接下來將一一說明。
1. 調整粗細：
    左上角顯示現在的筆粗和-+可以調整粗細。
2. 橡皮擦：
    按下Eraser可以根據筆觸清除。
3. 下載：
    按下Download即可下載現在圖片。
4. 清除：
    按下燃え尽きろ！即可清除畫布。
5. Undo/Redo：
    按下◀回到上一步，按下▶到下一步。
6. 調色盤：
    按下想要的顏色就可以換筆刷顏色！
7. 圖片上傳：
    首先從右上角上傳圖片，再按下P即可貼上圖片。
8. 輸入文字：
    先依序輸入大小、字形、想要顯示的字，再按T即可輸入文字。