var canvas = document.getElementById('canvas');
var context= canvas.getContext('2d');

var cPushArray = new Array();
var cStep = -1;
	
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    }
}

document.getElementById("canvas").style.cursor = "crosshair";

function init(){
    context.rect(0,0,canvas.width,canvas.height);
    context.fillStyle = "white";
    context.strokeStyle = "white";
    context.fill();
    cPush();
}

var radius = 10;
var dragging = false;


canvas.width = innerWidth;
canvas.height= innerHeight;

context.lineWidth = radius*2;

var putPoint = function(e){
    if(dragging){
        context.lineTo(e.clientX, e.clientY);
        context.stroke();
        context.beginPath();
        context.arc(e.clientX, e.clientY, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(e.clientX, e.clientY);
    }
}
    
var engage = function(e)
{
    dragging = true;
    context.moveTo(e.clientX,e.clientY);
    putPoint(e);
}
    
var disengage = function()
{
    dragging = false;
    context.beginPath();
    cPush();
}
    
canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);
